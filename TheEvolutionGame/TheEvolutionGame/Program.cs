﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Insects;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Insect insect = new Insect();
            Console.WriteLine(insect.Go());

            ICreature flyingInsect = Life.Evolve(insect, "Flying");
            Console.WriteLine(flyingInsect.Go());

        }
    }
}
