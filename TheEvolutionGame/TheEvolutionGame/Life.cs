﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame
{
    static class Life
    {
        public static ICreature Evolve
            (IEvolution creature,
            string type)
        {
            return creature.Evolve(type);
        }

        public static  ICreature DeEvolve
            (IEvolution creature)
        {
            return creature.DeEvolve();
        }
    }
}
