﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame
{
    abstract class Animal : ICreature
    {
        //de gasit metode pentru care sa punem si o implementare
        public abstract string Eat();

        public abstract string Go();

        public abstract string GoFast();

        public abstract string Sleep();
    }
}
