﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class FlyingInsect : Insect
    {
        public int FlyingSpeed { get; set; }

        public FlyingInsect()
        {

        }

        public FlyingInsect(string name, int numberOfLegs, int flyingSpeed) : base(name, numberOfLegs)
        {
            this.FlyingSpeed = flyingSpeed;
        }

        public override string Eat()
        {
            return "I need to eat in order to be able to fly!";
        }

        public override string Go()
        {
            return "I'm a flying insect and I'm walking";
        }

        public string Go(bool fly)
        {
            return fly ? Fly() : "I can fly, but know I'm just walking";
        }

        public override string GoFast()
        {
            return "It would be even faster to fly, so.." + Fly();
        }

        public virtual string Fly()
        {
            return "I'm flying";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }
    }
}
