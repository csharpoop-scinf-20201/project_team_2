﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class Insect : Animal, IInsect, IEvolution
    {
        public string Name { get; set; }
        public int NumberOfLegs { get; set; }

        public Insect()
        {
        }

        public Insect(string name)
        {
            this.Name = name;
        }

        public Insect(string name, int numberOfLegs)
        {
            this.Name = name;
            this.NumberOfLegs = numberOfLegs;
        }

        public override string Eat()
        {
            return "I'm a small insect but I'm hungry.";
        }

        public override string Go()
        {
            return "Walking insect!";
        }

        public override string GoFast()
        {
            return "Running insect!";
        }

        public override string Sleep()
        {
            return "Sleeping insect!";
        }

        public virtual string Pinch()
        {
            return "Be careful, maybe I can pinch you!";
        }
        
        public virtual ICreature Evolve(string type) // parametru pentru a determina catre ce clasa evoluam
        {
            Insect evolvedInsect = this;
            if(type == "Flying")
            {
                evolvedInsect = new FlyingInsect();
                return evolvedInsect;
            }
            else if(type == "NonFlying")
            {
                evolvedInsect = new NonFlyingInsect();
                return evolvedInsect;
            }

            Console.WriteLine("Unknown evolution type");
            return evolvedInsect;
        }

        public virtual ICreature DeEvolve()
        {
            Console.WriteLine("I can't de-evolve anymore");
            return this;
        }
    }
}
