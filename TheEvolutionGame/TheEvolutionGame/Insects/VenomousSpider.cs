﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class VenomousSpider : Spider
    {
        public int DangerLevel { get; set; }

        public VenomousSpider(string name,int numberOfLegs, bool talented, int dangerLevel) 
            : base(name, numberOfLegs ,talented)
        {
            this.DangerLevel = dangerLevel;
        }

        public override string Eat()
        {
            return "Venomous Spider eating pray";
        }

        public override string Go()
        {
            return "Venomous spider calmly walking";
        }

        public override string GoFast()
        {
            return "I can poison others, I'm not gonna run. I'm not afraid!";
        }

        public override string Sleep()
        {
            return "Let's get some rest to replenish the venom";
        }

        public override string Pinch()
        {
            return "Be careful, I can pinch you and I'm venomous! ";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }
    }
}
