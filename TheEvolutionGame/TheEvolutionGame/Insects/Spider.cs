﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class Spider : NonFlyingInsect
    {
        public bool TalentedSpiderWebDesigner { get; set; }

        public bool MyProperty { get; set; }
        public Spider(string name, int numberOfLegs, bool talented) : base(name,numberOfLegs,false)
        {
            this.TalentedSpiderWebDesigner = talented;
        }

        public override string Eat()
        {
            return "Let's check my spider web! I hope it caught something!";
        }

        public override string Go()
        {
            return "I'm using many legs to walk!";
        }

        public override string GoFast()
        {
            return "Also many legs to walk faster!";
        }

        public override string Sleep()
        {
            return "I will sleep until some little insects get caught in the web!";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }       
    }
}
