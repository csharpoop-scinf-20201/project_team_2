﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class Butterfly : FlyingInsect
    {
        public bool Colored { get; set; }

        public Butterfly(string name, int numberOfLegs, int flyingSpeed, bool colored) 
            : base(name, numberOfLegs, flyingSpeed)
        {
            this.Colored = colored;
        }

        public override string Eat()
        {
            return "Butterflies also like flowers! What a great taste!";
        }

        public override string Go()
        {
            return "Walking butterfly, would better fly";
        }

        public override string GoFast()
        {
            return "Let's run from here!";
        }

        public override string Sleep()
        {
            return "Let's find a place to sleep";
        }

        public override string Fly()
        {
            return "Gracefully flying butterfly!";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }

    }
}
