﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class Bee : FlyingInsect
    {
        public bool Lazy { get; set; }

        public Bee()
        {

        }

        public Bee(string name, int numberOfLegs, int flyingSpeed, bool lazy) 
            : base(name, numberOfLegs, flyingSpeed)
        {
            this.Lazy = lazy;
        }

        public override string Eat()
        {
            return "What a delicious flower!";
        }

        public override string Go()
        {
            return "Let's go to another flower!";
        }

        public override string GoFast()
        {
            return "Bees in danger, run!";
        }

        public override string Sleep()
        {
            return "This flower smells so good! I will sleep here!";
        }

        public override string Fly()
        {
            return "I'm flying from flower to flower! ";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }
    }
}
