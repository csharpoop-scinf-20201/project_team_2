﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class Ant : NonFlyingInsect
    {
        public string Color { get; set; }

        public Ant(string name, int numberOfLegs, string color) : base(name, numberOfLegs, false)
        {
            this.Color = color;
        }

        public override string Eat()
        {
            return "Even if I'm a little ant, yes, I still can eat";
        }

        public override string Go()
        {
            return "I'm going to search some food! ";
        }

        public override string GoFast()
        {
            return "Oh, no, Spider is coming! Run! ";
        }

        public override string Sleep()
        {
            return "Sleepy ant ZzZzzZzZ ";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }
    }
}
