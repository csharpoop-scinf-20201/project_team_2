﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheEvolutionGame.Interfaces;

namespace TheEvolutionGame.Insects
{
    class NonFlyingInsect : Insect
    {
        public bool CanJump { get; set; }

        public NonFlyingInsect()
        {

        }
        public NonFlyingInsect(string name, int numberOfLegs, bool canJump) : base(name, numberOfLegs)
        {
            this.CanJump = canJump;
        }

        public override string Eat()
        {
            return "It's hard for me to catch the pray as long as I'm not able to fly";
        }

        public override string Go()
        {
            if (CanJump)
            {
                return "Jumping insect";
            }
            else
            {
                return "Can not fly or jump so I'm just walking";
            }
        }

        public override string GoFast()
        {
            return "I have to run!! I would have liked to fly!";
        }

        public override string Sleep()
        {
            return "I go to sleep. Maybe I will dream that I'm flying!";
        }

        public override ICreature Evolve(string type)
        {
            throw new NotImplementedException();
        }

        public override ICreature DeEvolve()
        {
            throw new NotImplementedException();
        }

    }
}
