﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Interfaces
{
    interface IEvolution
    {
        ICreature Evolve(string type);
        ICreature DeEvolve();
    }
}
