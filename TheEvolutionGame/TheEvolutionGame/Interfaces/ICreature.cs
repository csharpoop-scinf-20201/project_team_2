﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Interfaces
{
    interface ICreature
    {
        string Go();
        string GoFast();
        string Eat();
        string Sleep();
    }
}
