﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheEvolutionGame.Interfaces
{
    interface IInsect : ICreature
    {
        string Pinch();
    }
}
